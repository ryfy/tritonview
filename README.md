# Triton View Styleguide

Styled components for the Triton View application

## Requirements

To launch this repo locally, you need:

- [NodeJS](https://nodejs.org/en/) (0.12 or greater)
- [Git](https://git-scm.com/)
- [Bower](http://bower.io/)

## Installation & Setup

- run 'npm install' to get all required node packages
- run 'bower install' to get needed vendor packages
- put any external js/css references inside the gulpfile.js
- run 'gulp' and let it listen and compile