//Setup foundation stuff
$(document).foundation();

// On load
$(window).load(function() {
	init();
});

// On window resize
$(window).resize(function() {
    sizeLeftTabPanel();
    sizeRightPanel();
    sizeLeftPanel();
});

// On tab change
$('#triton-tabs').on('change.zf.tabs', function() {
    sizeLeftTabPanel();
});

function sizeLeftTabPanel(){

	$tabContentHeight = $('#left-tabs .panel-content').height() - $('#left-tabs .tabs').outerHeight();
	$('#left-tabs .tabs-content').outerHeight($tabContentHeight);

	// size Polygons tab table
	if(!$('#tab-polygons').hasClass('is-active')){
		// tab has to be active before we can calculate heights
		$('#tab-polygons').addClass('is-active');
		$afterPanelInfoHeight = $tabContentHeight - $('#tab-polygons .panel-info ul').outerHeight();
		if(document.getElementById("polygon-controls") !== null){
			sizePolygonControls($afterPanelInfoHeight);
		}
		$('#tab-polygons').removeClass('is-active');
	}else{
		$afterPanelInfoHeight = $tabContentHeight - $('#tab-polygons .panel-info ul').outerHeight();
		sizePolygonControls($afterPanelInfoHeight);
	}

	$afterPanelInfoAndPaginateToolbarHeight = $afterPanelInfoHeight - $('#tab-polygons .paginate.toolbar').outerHeight();
	$('#tab-polygons .table-wrap').outerHeight($afterPanelInfoAndPaginateToolbarHeight);


	// size Display tab tables
	$table1DisplayHeight = $tabContentHeight * .60;
	$table2DisplayHeight = $tabContentHeight * .40;

	// subtract thead height
	$table1DisplayHeight = $table1DisplayHeight - $('#tab-display #surveys.table-wrap thead').outerHeight();
	$table2DisplayHeight = $table2DisplayHeight - $('#tab-display #culture.table-wrap thead').height();

	$('#tab-display #surveys.table-wrap tbody').height($table1DisplayHeight);
	$('#tab-display #culture.table-wrap tbody').height($table2DisplayHeight);

}

function sizePolygonControls($sectionHeight){
	$controlsWidth = $('#polygon-controls')[0].getBoundingClientRect().width;

	// size Polygon tab buttons
	$btnSize = $controlsWidth/2; // getBoundingClientRect() returns floating point width
	$('#polygon-controls .button').width($btnSize).height($btnSize);

	//size Polygon tab control bars
	$barTotal = $('#polygon-controls .control-bars .bar').length;
	$controlBarsHeight = $sectionHeight - $btnSize;

	$('#polygon-controls .control-bars .bar').height($controlBarsHeight/$barTotal);

}

function sizeLeftPanel(){
	$contentHeight = $('#left-canvas').height() - $('#left-canvas .panel-info').height();
	$('#left-canvas .panel-content').height($contentHeight);
}

function sizeRightPanel(){
	$contentHeight = $('#right-canvas').height() - $('#right-canvas .panel-info').height();
	$('#right-canvas .panel-content').height($contentHeight);
}

function initToolbars(){
	//set click events for button toolbar
	$('.flyout').on('click', function(){
		$('.options', this).toggleClass('is-active');
	});

	//set click events for buttons
	$('.button.toggle').on('click', function(){
		$(this).parent().find('.is-active').toggleClass('is-active');
		$(this).toggleClass('is-active');
	});

	//color picker flyout
	$('.toolbar .flyout .options .button').on('click', function(){
		$('.toolbar .flyout').attr('data-color', $(this).data('color'));
	});
}

function initTableFilters() {
	//set click events for Display table filters
	$('.options td a').on('click', function(){
		$(this).toggleClass('selected');
	});
}

function updateRangeSlider(val) {
	var vertical = true;
	/* setup variables for the elements of our slider */
	var thumb = document.getElementById("sliderthumb");
	var slider = document.getElementById("slider");

	var pc = val/(slider.max - slider.min); /* the percentage slider value */
	var thumbsize = 16; /* must match the thumb size in your css */
	var bigval = 209; /* widest or tallest value depending on orientation */
	var smallval = 14; /* narrowest or shortest value depending on orientation */
	var tracksize = bigval - thumbsize;
	var loc = vertical ? (1 - pc) * tracksize : pc * tracksize;

	thumb.style.top =  (vertical ? loc : 0) + "px";
}

function initRangeSlider() {
	if(document.getElementById("slider") !== null){
		val = document.getElementById("slider").value;
		updateRangeSlider(val);
	}
}

function initNav(){
	// to toggle right pane into new window
	// pausing on this. not 100% of intention
	$('#nav .screen-toggle').click(function(){
	  window.open('pop-out.php', 'Triton View', 'window settings');

	  $('#right-canvas').parent().hide();
	  $('#left-canvas').parent().width('100%');
	  $('#left-tabs').width('50%');

	  return false;
	});
}

function init(){
	sizeLeftTabPanel();
	sizeRightPanel();
	sizeLeftPanel();
	initTableFilters();
	initToolbars();
	initRangeSlider();
	//initNav();
}
