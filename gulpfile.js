var gulp = require('gulp');
$ = require('gulp-load-plugins')();
var cleanCSS = require('gulp-clean-css');
var es = require('event-stream');

var sassPaths = [
  'bower_components/foundation-sites/scss',
  'bower_components/motion-ui/src'
];

var jsPaths = [
  'bower_components/jquery/dist/jquery.js',
  'bower_components/what-input/what-input.js',
  'bower_components/foundation-sites/dist/foundation.js',
  'js/app.js'
];

var cssPaths = [

];

//Create css stuff
gulp.task('css', function(){
  var sassFiles = gulp.src('scss/app.scss')
    /* compile sass files */
    .pipe($.sass({
     includePaths: sassPaths
    }));

    return es.concat(gulp.src(cssPaths), sassFiles)
    /* combine output of sass compile with other css files */
    .pipe($.concat('all.css'))
    /* autoprefix */
    .pipe($.autoprefixer({
     browsers: ['last 2 versions', 'ie >= 9']
    }))
    /* minify */
    .pipe(cleanCSS({compatibility: 'ie8', keepSpecialComments: '0'}))
    /* write file */
    .pipe(gulp.dest('css'));
});


//Combine and minimize all the JS
gulp.task('scripts', function() {
  return gulp.src(jsPaths)
    .pipe($.concat('all.js'))
    .pipe($.uglify())
    .pipe(gulp.dest('js/'));
});


gulp.task('default', ['css', 'scripts'], function() {
  gulp.watch(['scss/**/*.scss'], ['css']);
  gulp.watch(['js/app.js'], ['scripts']);
});